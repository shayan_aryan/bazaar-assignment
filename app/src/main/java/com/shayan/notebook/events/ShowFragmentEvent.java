package com.shayan.notebook.events;

import com.shayan.notebook.view.fragment.base.BaseFragment;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(suppressConstructorProperties = true)
@Getter
public class ShowFragmentEvent {
    private BaseFragment fragment;
}
