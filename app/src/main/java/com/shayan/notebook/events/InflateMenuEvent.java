package com.shayan.notebook.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(suppressConstructorProperties = true)
public class InflateMenuEvent {

    public static final int NONE = 0;

    private int menuRes;
}
