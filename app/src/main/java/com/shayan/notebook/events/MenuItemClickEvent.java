package com.shayan.notebook.events;

import android.view.MenuItem;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(suppressConstructorProperties = true)
@Getter
public class MenuItemClickEvent {
    private MenuItem item;
}
