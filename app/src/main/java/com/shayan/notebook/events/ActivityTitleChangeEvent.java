package com.shayan.notebook.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(suppressConstructorProperties = true)
public class ActivityTitleChangeEvent {
    //can be string res or String itself
    @Getter
    private Object title;

}
