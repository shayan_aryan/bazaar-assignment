package com.shayan.notebook.di.component;

import com.shayan.notebook.adapter.NotesAdapter;
import com.shayan.notebook.application.App;
import com.shayan.notebook.di.module.ServiceModule;
import com.shayan.notebook.view.fragment.NoteFragment;
import com.shayan.notebook.viewmodel.BackupRestoreViewModel;
import com.shayan.notebook.viewmodel.DrawingViewModel;
import com.shayan.notebook.viewmodel.FoldersViewModel;
import com.shayan.notebook.viewmodel.NoteListViewModel;
import com.shayan.notebook.viewmodel.NoteViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(NoteFragment noteFragment);

    void inject(FoldersViewModel foldersViewModel);

    void inject(NoteListViewModel noteListViewModel);

    void inject(NoteViewModel noteViewModel);

    void inject(DrawingViewModel drawingViewModel);

    void inject(BackupRestoreViewModel backupRestoreViewModel);

    void inject(NotesAdapter notesAdapter);

    void inject(App app);

}
