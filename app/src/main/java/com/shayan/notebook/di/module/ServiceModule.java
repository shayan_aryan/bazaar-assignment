package com.shayan.notebook.di.module;

import android.content.Context;

import com.shayan.notebook.application.Config;
import com.shayan.notebook.db.DataBaseManager;
import com.shayan.notebook.helper.PreferenceHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
@Singleton
public class ServiceModule {

    private Context context;

    public ServiceModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context providesContext() {
        return context.getApplicationContext();
    }

    @Singleton
    @Provides
    Config providesConfig(Context context) {
        return new Config(context);
    }

    @Singleton
    @Provides
    DataBaseManager provideDataBaseManager(Context context, Config config) {
        return new DataBaseManager(context, config);
    }

    @Singleton
    @Provides
    PreferenceHelper providePreferenceHelper(Context context) {
        return new PreferenceHelper(context);
    }
}
