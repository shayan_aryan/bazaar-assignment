package com.shayan.notebook.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shayan.notebook.R;
import com.shayan.notebook.model.NoteFolder;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FoldersAdapter extends RecyclerView.Adapter<FoldersAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private List<NoteFolder> items;


    public FoldersAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_folder, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NoteFolder item = getItem(position);
        holder.name.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public NoteFolder getItem(int position) {
        return items.get(position);
    }

    public void updateItems(List<NoteFolder> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void addItem(NoteFolder noteFolder) {
        items.add(0, noteFolder);
        notifyItemInserted(0);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.folder_name)
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
