package com.shayan.notebook.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shayan.notebook.R;
import com.shayan.notebook.application.App;
import com.shayan.notebook.application.Config;
import com.shayan.notebook.helper.DateHelper;
import com.shayan.notebook.model.NoteItem;
import com.shayan.notebook.view.widget.LoadImageView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shayan on 8/15/16.
 */
public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    private List<NoteItem> items;

    @Inject
    Config config;

    public NotesAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        App.get(context).getServiceComponent().inject(this);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_note, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NoteItem item = getItem(position);

        if (!TextUtils.isEmpty(item.getTitle())) {
            holder.title.setText(item.getTitle());
            holder.title.setVisibility(View.VISIBLE);
        } else {
            holder.title.setVisibility(View.GONE);
        }

        if (item.getContent() != null) {
            holder.content.setText(item.getContent());
            holder.content.setVisibility(View.VISIBLE);
        } else {
            holder.content.setVisibility(View.INVISIBLE);
        }

        if (item.getCreatedDate() > 0) {
            CharSequence dateString = DateHelper.getRelativeTimeString(item.getCreatedDate());
            holder.date.setText(context.getString(R.string.created, dateString));
        }

        if (item.hasDrawing(config)) {
            holder.image.setVisibility(View.VISIBLE);
            holder.image.loadImage(item.getDrawingPath(config));
        } else {
            holder.image.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public NoteItem getItem(int position) {
        return items.get(position);
    }

    public void updateItems(List<NoteItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.note_item_title)
        TextView title;
        @Bind(R.id.note_item_content)
        TextView content;
        @Bind(R.id.note_item_date)
        TextView date;
        @Bind(R.id.note_item_image)
        LoadImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
