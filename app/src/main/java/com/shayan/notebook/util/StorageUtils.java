package com.shayan.notebook.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import com.shayan.notebook.application.Config;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import rx.Observable;
import rx.subjects.BehaviorSubject;

public class StorageUtils {

    public static Observable<Integer> saveBitmapToFile(Bitmap bitmap, String path) {
        BehaviorSubject<Integer> subject = BehaviorSubject.create();

        runInAsyncTask(() -> {
            try {
                File tempfile = new File(path);
                FileOutputStream outputStream = new FileOutputStream(tempfile);

                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

                outputStream.flush();
                outputStream.close();
                bitmap.recycle();
            } catch (IOException e) {
                e.printStackTrace();
            }

            subject.onNext(1);
        });

        return subject;
    }

    public static Observable<Bitmap> getBitmapFromFile(Context context, String path) {
        Uri uri = Uri.fromFile(new File(path));
        return getBitmapFromUri(context, uri);
    }

    public static Observable<Bitmap> getBitmapFromUri(Context context, Uri uri) {
        BehaviorSubject<Bitmap> subject = BehaviorSubject.create();

        runInAsyncTask(() -> {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                subject.onNext(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
                subject.onError(e);
            }
        });

        return subject;
    }

    public static Observable<File> backupAppDataDirectory(Context context, Config config) {
        File[] folders = context.getFilesDir().getParentFile().listFiles();
//        ArrayList<File> foldersList = new ArrayList<>();
//        foldersList.addAll(Arrays.asList(folders));

        BehaviorSubject<File> subject = BehaviorSubject.create();

        runInAsyncTask(() -> {
            try {
                ZipFile zipFile = new ZipFile(config.getBackupFilePath(context));
                for (File folder : folders) {
                    zipFile.addFolder(folder, getZipParameters(config));
                }
                subject.onNext(zipFile.getFile());
            } catch (ZipException e) {
                e.printStackTrace();
                subject.onError(e);
            }
        });

        return subject;
    }

    public static Observable<Integer> restoreAppDataFromFile(Context context, String zipFilePath, Config config) {
        BehaviorSubject<Integer> subject = BehaviorSubject.create();
        String dataPath = context.getFilesDir().getParent();

        runInAsyncTask(() -> {
            try {
                clearApplicationData(context);
                ZipFile zipFile = new ZipFile(zipFilePath);
                if (zipFile.isEncrypted()) {
                    zipFile.setPassword(config.getArchivePass());
                    zipFile.extractAll(dataPath);
                    subject.onNext(1);
                } else {
                    throw new ZipException("Not Encrypted File");
                }
            } catch (ZipException e) {
                e.printStackTrace();
                subject.onError(e);
            }
        });

        return subject;
    }

    private static ZipParameters getZipParameters(Config config) {
        ZipParameters parameters = new ZipParameters();
        parameters.setEncryptFiles(true);
        parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
        parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
        parameters.setPassword(config.getArchivePass());
        return parameters;
    }

    public static void clearApplicationData(Context context) {
        File cacheDirectory = context.getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib"))
                    deleteFile(new File(applicationDirectory, fileName));
            }
        }
    }

    public static boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }
        return deletedAll;
    }

    private static void runInAsyncTask(Runnable runnable) {
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String[] params) {
                runnable.run();
                return null;
            }
        }.execute();
    }
}
