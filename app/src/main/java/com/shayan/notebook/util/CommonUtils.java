package com.shayan.notebook.util;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.TextView;

import com.jakewharton.processphoenix.ProcessPhoenix;
import com.shayan.notebook.view.activity.MainActivity;

public class CommonUtils {

    public static boolean isEmpty(TextView etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    public static void restartApp(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        ProcessPhoenix.triggerRebirth(context, intent);
    }

    public static Intent createPickFileIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        return intent;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        if (contentUri.getPath().startsWith("/")) {
            return contentUri.getPath();
        } else {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }
    }
}
