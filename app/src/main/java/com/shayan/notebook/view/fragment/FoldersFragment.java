package com.shayan.notebook.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shayan.notebook.R;
import com.shayan.notebook.adapter.FoldersAdapter;
import com.shayan.notebook.databinding.FragmentFoldersBinding;
import com.shayan.notebook.events.ShowFragmentEvent;
import com.shayan.notebook.helper.SwipeCallback;
import com.shayan.notebook.helper.recyclerview.GridAutoFitLayoutManager;
import com.shayan.notebook.helper.recyclerview.RecyclerItemClickListener;
import com.shayan.notebook.model.NoteFolder;
import com.shayan.notebook.view.fragment.base.BaseFragment;
import com.shayan.notebook.viewmodel.FoldersViewModel;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.NoArgsConstructor;
import ui.android.dialogalchemy.DialogAlchemy;
import ui.android.dialogalchemy.Material;
import ui.android.dialogalchemy.PhilosopherStone;
import ui.android.dialogalchemy.stone.EditTextStone;

@NoArgsConstructor
public class FoldersFragment extends BaseFragment implements FoldersViewModel.DataListener {

    private FragmentFoldersBinding binding;
    private FoldersViewModel viewModel;

    private FoldersAdapter foldersAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_folders, container, false);
        ButterKnife.bind(this, binding.getRoot());
        initViews();

        viewModel = new FoldersViewModel(getActivity(), this);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    private void initViews() {
        int gridItemWidth = getResources().getDimensionPixelSize(R.dimen.folder_grid_column_width);

        RecyclerView recyclerView = binding.rvFolders;
        recyclerView.setLayoutManager(new GridAutoFitLayoutManager(getActivity(), gridItemWidth));
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), (view, position) -> {
                    NoteFolder item = foldersAdapter.getItem(position);
                    showNoteListFragment(item);
                })
        );

        foldersAdapter = new FoldersAdapter(getActivity());
        recyclerView.setAdapter(foldersAdapter);

        ItemTouchHelper touchHelper = new ItemTouchHelper(new SwipeCallback(position -> {
            NoteFolder item = foldersAdapter.getItem(position);
            viewModel.deleteFolder(item.getId());
            foldersAdapter.removeItem(position);
        }));
        touchHelper.attachToRecyclerView(recyclerView);
    }

    private void showNoteListFragment(NoteFolder item) {
        NoteListFragment noteListFragment = NoteListFragment.create(item);
        EventBus.getDefault().post(new ShowFragmentEvent(noteListFragment));
    }

    @Override
    public void onDataChanged(List<NoteFolder> noteFolders) {
        foldersAdapter.updateItems(noteFolders);
    }

    @Override
    public void onFolderAdded(NoteFolder noteFolder) {
        foldersAdapter.addItem(noteFolder);
    }

    @OnClick(R.id.folder_add_btn)
    public void onAddFolderClicked(View view) {
        showFolderDialog(null);
    }

    private void showFolderDialog(@Nullable String folderName) {
        if (folderName == null) {
            folderName = "";
        }

        PhilosopherStone stone = new EditTextStone.Builder()
                .setText(folderName)
                .setOnTextAcceptedListener(name -> {
                    if (!TextUtils.isEmpty(name)) {
                        addFolder(name);
                    }
                })
                .build();

        Material material = new Material.Builder(getActivity())
                .setTitle(R.string.create_folder)
                .setMessage(R.string.write_folder_name)
                .setPositiveButton(R.string.create, null)
                .setNegativeButton(R.string.cancel, null)
                .setPhilosopherStone(stone)
                .build();

        DialogAlchemy.show(getFragmentManager(), material);
    }

    private void addFolder(String folderName) {
        viewModel.addFolder(folderName);
    }

    @Override
    public void onResume() {
        super.onResume();
        changeActivityTitle(R.string.folders);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewModel.onDestroy();
    }
}
