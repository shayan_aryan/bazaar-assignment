package com.shayan.notebook.view;

import android.graphics.Path;

public class CustomPath extends Path {

    private float lastX, lastY;

    public void start(float x, float y) {
        moveTo(x, y);
        lastX = x;
        lastY = y;
    }

    public void move(float x, float y) {
        quadTo(lastX, lastY, (x + lastX) / 2, (y + lastY) / 2);
        lastX = x;
        lastY = y;
    }

    public void end(float x, float y) {
        lineTo(x, y);
    }
}
