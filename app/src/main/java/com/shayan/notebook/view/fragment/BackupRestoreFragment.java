package com.shayan.notebook.view.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shayan.notebook.R;
import com.shayan.notebook.databinding.FragmentBackupRestoreBinding;
import com.shayan.notebook.util.CommonUtils;
import com.shayan.notebook.util.RxUtils;
import com.shayan.notebook.view.fragment.base.BaseFragment;
import com.shayan.notebook.viewmodel.BackupRestoreViewModel;
import com.tbruyelle.rxpermissions.RxPermissions;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.NoArgsConstructor;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@NoArgsConstructor
public class BackupRestoreFragment extends BaseFragment implements BackupRestoreViewModel.DataListener {

    private static final int PICK_BACKUP_FILE_REQUEST_CODE = 80;

    private BackupRestoreViewModel viewModel;
    private FragmentBackupRestoreBinding binding;

    @Bind(R.id.message_view)
    TextView messageView;
    @Bind(R.id.loading_view)
    ProgressBar loadingProgress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_backup_restore, container, false);
        ButterKnife.bind(this, binding.getRoot());

        viewModel = new BackupRestoreViewModel(getActivity(), this, lifecycleSubject);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @OnClick(R.id.btn_backup)
    public void onBackupClicked() {
        RxPermissions.getInstance(getActivity())
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(hasPermission -> {
                    if (hasPermission) {
                        backup();
                    }
                });
    }

    @OnClick(R.id.btn_restore)
    public void onRestoreClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

            RxPermissions.getInstance(getActivity())
                    .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(hasPermission -> {
                        if (hasPermission)
                            pickBackupFile();
                    });
        } else {
            pickBackupFile();
        }
    }

    private void backup() {
        showProgressBar();
        viewModel.backupAppData();
    }

    private void restore(Uri fileUri) {
        showProgressBar();
        viewModel.restoreAppData(fileUri);
    }

    @Override
    public void onBackupCompleted(boolean success, String message) {
        hideProgressBar();
        showMessage(success, message);
    }

    @Override
    public void onRestoreCompleted(boolean success, String message) {
        hideProgressBar();
        showMessage(success, message);

        if (success) {
            //restart app to recreate connections to file system
            RxUtils.runDelayed(1000, () -> CommonUtils.restartApp(getActivity()));
        }
    }

    private void pickBackupFile() {
        startActivityForResult(CommonUtils.createPickFileIntent(), PICK_BACKUP_FILE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_BACKUP_FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            restore(data.getData());
        }
    }

    private void showMessage(boolean success, String message) {
        int colorRes = success ? R.color.green : R.color.red;
        messageView.setTextColor(ContextCompat.getColor(getActivity(),colorRes));
        messageView.setText(message);
    }

    private void showProgressBar() {
        loadingProgress.setVisibility(View.VISIBLE);
        messageView.setVisibility(View.INVISIBLE);
    }

    private void hideProgressBar() {
        loadingProgress.setVisibility(View.INVISIBLE);
        messageView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        clearActivityMenu();
        changeActivityTitle(R.string.backup_restore);
    }
}
