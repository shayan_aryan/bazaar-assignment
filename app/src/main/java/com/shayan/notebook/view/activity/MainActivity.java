package com.shayan.notebook.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.shayan.notebook.R;
import com.shayan.notebook.events.ActivityTitleChangeEvent;
import com.shayan.notebook.events.InflateMenuEvent;
import com.shayan.notebook.events.MenuItemClickEvent;
import com.shayan.notebook.events.ShowFragmentEvent;
import com.shayan.notebook.view.activity.base.BaseActivity;
import com.shayan.notebook.view.fragment.BackupRestoreFragment;
import com.shayan.notebook.view.fragment.FoldersFragment;
import com.shayan.notebook.view.fragment.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        addFragment(new FoldersFragment());
    }

    /**
     * Called from fragments
     */
    @Subscribe
    public void onShowFragmentEvent(ShowFragmentEvent event) {
        addFragment(event.getFragment());
    }

    /**
     * Called from fragments to change toolbar's title
     */
    @Subscribe
    public void onActivityTitleChangeEvent(ActivityTitleChangeEvent event) {
        Object title = event.getTitle();
        if (title instanceof String)
            setToolbarTitle((String) title);
        else if (title instanceof Integer)
            setToolbarTitle((Integer) title);
    }

    /**
     * Called from fragments to change the toolbar's menu
     */
    @Subscribe
    public void onInflateMenuEvent(InflateMenuEvent event) {
        toolbar.getMenu().clear();
        if (event.getMenuRes() != InflateMenuEvent.NONE) {
            toolbar.inflateMenu(event.getMenuRes());
        }
    }

    public void addFragment(BaseFragment fragment) {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            enableBackButton();
        }

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(fragment.getClass().getSimpleName())
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.fragment_container, fragment)
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            //if this is the first fragment, we hide the back icon
            if (getSupportFragmentManager().getBackStackEntryCount() == 2) {
                disableBackButton();
                toolbar.inflateMenu(R.menu.main_menu);
            }

            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_backup_restore:
                BackupRestoreFragment backupRestoreFragment = new BackupRestoreFragment();
                addFragment(backupRestoreFragment);
                break;

            default:
                //publish menu click to fragments
                EventBus.getDefault().post(new MenuItemClickEvent(menuItem));
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //pas the result to fragments
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
}
