package com.shayan.notebook.view.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.widget.EditText;

import lombok.Getter;

public class NoteEditText extends EditText {

    @Getter
    private boolean boldEnabled, italicEnabled;

    public NoteEditText(Context context) {
        super(context);
        init();
    }

    public NoteEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NoteEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (isInEditMode())
            return;

    }

    public void changeBoldState() {
        int start = getSelectionStart();
        int end = getSelectionEnd();

        if (start != end) {
            //text is selected
            if (!isBold(start, end))
                setBold(start, end);
            else
                setRegular(start, end);

        } else {
            //no text is selected, enabling bold mode
            boldEnabled = !boldEnabled;
        }
    }

    public void changeItalicState() {
        int start = getSelectionStart();
        int end = getSelectionEnd();

        if (start != end) {
            //text is selected
            if (!isItalic(start, end))
                setItalic(start, end);
            else
                setRegular(start, end);

        } else {
            //no text is selected, enabling italic mode
            italicEnabled = !italicEnabled;
        }
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);

        if (boldEnabled) {
            setBold(start, start + lengthAfter);
        }

        if (italicEnabled) {
            setItalic(start, start + lengthAfter);
        }
    }

    private void setBold(int start, int end) {
        getText().setSpan(new StyleSpan(Typeface.BOLD), start, end, 0);
    }

    private void setItalic(int start, int end) {
        getText().setSpan(new StyleSpan(Typeface.ITALIC), start, end, 0);
    }

    private void setRegular(int start, int end) {
        Spannable spannable = getText();
        //remove style spans
        StyleSpan[] styleSpans = getStyleSpans(start, end);
        for (StyleSpan s : styleSpans) {
            spannable.removeSpan(s);
        }

    }

    private boolean isBold(int start, int end) {
        StyleSpan[] styleSpans = getStyleSpans(start, end);
        for (StyleSpan s : styleSpans) {
            if (s.getStyle() == Typeface.BOLD)
                return true;
        }

        return false;
    }

    private boolean isItalic(int start, int end) {
        StyleSpan[] styleSpans = getStyleSpans(start, end);
        for (StyleSpan s : styleSpans) {
            if (s.getStyle() == Typeface.ITALIC)
                return true;
        }

        return false;
    }

    private StyleSpan[] getStyleSpans(int start, int end) {
        return getText().getSpans(start, end, StyleSpan.class);
    }

    private UnderlineSpan[] getUnderlineSpans(int start, int end, Spannable spannable) {
        return spannable.getSpans(start, end, UnderlineSpan.class);
    }

    @Override
    public Editable getText() {
        Spannable spannable = super.getText();

        //remove underline spans
        UnderlineSpan[] underlineSpans = getUnderlineSpans(0, spannable.length(), spannable);
        for (UnderlineSpan span : underlineSpans) {
            spannable.removeSpan(span);
        }

        return super.getText();
    }
}
