package com.shayan.notebook.view.fragment.base;

import android.support.annotation.CallSuper;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

import com.shayan.notebook.events.ActivityTitleChangeEvent;
import com.shayan.notebook.events.InflateMenuEvent;
import com.trello.rxlifecycle.FragmentEvent;

import org.greenrobot.eventbus.EventBus;

import rx.subjects.BehaviorSubject;

public class BaseFragment extends Fragment {

    protected void changeActivityTitle(@StringRes int titleRes) {
        EventBus.getDefault().post(new ActivityTitleChangeEvent(titleRes));
    }

    protected void changeActivityTitle(String title) {
        EventBus.getDefault().post(new ActivityTitleChangeEvent(title));
    }

    protected void inflateActivityMenu(int menuRes) {
        EventBus.getDefault().post(new InflateMenuEvent(menuRes));
    }

    protected void clearActivityMenu() {
        EventBus.getDefault().post(new InflateMenuEvent(InflateMenuEvent.NONE));
    }

    /**
     * Subject used for handling life cycles of rx subscriptions
     */
    protected final BehaviorSubject<FragmentEvent> lifecycleSubject = BehaviorSubject.create();

    @CallSuper
    @Override
    public void onPause() {
        lifecycleSubject.onNext(FragmentEvent.PAUSE);
        super.onPause();
    }

    @CallSuper
    @Override
    public void onStop() {
        lifecycleSubject.onNext(FragmentEvent.STOP);
        super.onStop();
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        lifecycleSubject.onNext(FragmentEvent.DESTROY_VIEW);
        super.onDestroyView();
    }

    @CallSuper
    @Override
    public void onDestroy() {
        lifecycleSubject.onNext(FragmentEvent.DESTROY);
        super.onDestroy();
    }

}
