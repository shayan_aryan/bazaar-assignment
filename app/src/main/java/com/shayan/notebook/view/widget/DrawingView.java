package com.shayan.notebook.view.widget;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;

import com.shayan.notebook.R;
import com.shayan.notebook.view.CustomPath;

import java.util.ArrayList;
import java.util.List;

import lombok.Setter;


public class DrawingView extends LoadImageView {

    @Setter
    private Mode mode;

    private CustomPath currentPath;
    private Paint brushPaint, erasePaint;

    private List<Pair<Path, Paint>> paths = new ArrayList<>();

    private List<Pair<Path, Paint>> unDonePaths = new ArrayList<>();

    private boolean undone = false;

    private Bitmap backGroundBitmap;
    private int width, height;

    public enum Mode {
        PAINTING,
        ERASING
    }

    public DrawingView(Context context) {
        super(context);
        init();
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        float brushSize = getResources().getInteger(R.integer.paint_brush_size);
        int paintColor = ContextCompat.getColor(getContext(), R.color.brush_default_color);

        brushPaint = new Paint();
        brushPaint.setColor(paintColor);
        brushPaint.setStrokeWidth(brushSize);
        brushPaint.setStyle(Paint.Style.STROKE);
        brushPaint.setStrokeJoin(Paint.Join.ROUND);
        brushPaint.setStrokeCap(Paint.Cap.ROUND);
        brushPaint.setPathEffect(new CornerPathEffect(10));   // set the path effect when they join.
        brushPaint.setAntiAlias(true);

        erasePaint = new Paint();
        erasePaint.setColor(getResources().getColor(R.color.white));
        erasePaint.setAntiAlias(true);
        erasePaint.setStrokeWidth(getResources().getInteger(R.integer.erase_brush_size));
        erasePaint.setStyle(Paint.Style.STROKE);
        erasePaint.setStrokeJoin(Paint.Join.ROUND);
        erasePaint.setStrokeCap(Paint.Cap.ROUND);
        erasePaint.setPathEffect(new CornerPathEffect(10));   // set the path effect when they join.
        erasePaint.setAntiAlias(true);

        mode = Mode.PAINTING;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (backGroundBitmap != null) {
            canvas.drawBitmap(backGroundBitmap, 0, 0, null);
        }

        for (int i = 0; i < paths.size(); i++) {
            canvas.drawPath(paths.get(i).first, paths.get(i).second);
        }
    }

//    public boolean onTouch(View v, MotionEvent event) {
//        return false;
//    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (undone) {
            clearUndoStack();
        }

        Paint currentPaint;
        switch (mode) {
            case PAINTING:
                currentPaint = brushPaint;
                break;
            case ERASING:
                currentPaint = erasePaint;
                break;
            default:
                currentPaint = brushPaint;
        }

        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                currentPath = new CustomPath();
                paths.add(new Pair<>(currentPath, currentPaint));
                currentPath.start(x, y);
                break;

            case MotionEvent.ACTION_MOVE:
                currentPath.move(x, y);

                break;
            case MotionEvent.ACTION_UP:
                currentPath.end(x, y);
                break;

            default:
                return false;
        }
        //draw
        invalidate();
        return true;
    }

    public Bitmap getDrawing() {
        if (paths.size() == 0)
            return null;

        if (backGroundBitmap == null) {
            backGroundBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(backGroundBitmap);
        for (Pair<Path, Paint> p : paths) {
            canvas.drawPath(p.first, p.second);
        }
        return backGroundBitmap;
    }

    public void undo() {
        if (paths.size() > 0) {
            unDonePaths.add(paths.remove(paths.size() - 1));
            undone = true;
            invalidate();
        }
    }

    public void redo() {
        if (unDonePaths.size() > 0) {
            paths.add(unDonePaths.remove(unDonePaths.size() - 1));
            invalidate();
        }
    }

    public void clear() {
        paths.clear();
        unDonePaths.clear();
        backGroundBitmap = null;
        setImageBitmap(null);
        invalidate();
    }

    private void clearUndoStack() {
        unDonePaths.clear();
        undone = false;
    }

    @Override
    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);

        if (bitmap == null)
            return;

        if (!bitmap.isMutable()) {
            android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();
            // set default bitmap config if none
            if (bitmapConfig == null) {
                bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
            }
            bitmap = bitmap.copy(bitmapConfig, true);
        }

        this.backGroundBitmap = bitmap;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        width = View.MeasureSpec.getSize(widthMeasureSpec);
        height = View.MeasureSpec.getSize(heightMeasureSpec);

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unDonePaths.clear();
        paths.clear();
    }

}

