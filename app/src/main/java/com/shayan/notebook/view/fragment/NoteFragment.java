package com.shayan.notebook.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.shayan.notebook.R;
import com.shayan.notebook.application.App;
import com.shayan.notebook.application.Config;
import com.shayan.notebook.databinding.FragmentNoteBinding;
import com.shayan.notebook.events.DrawingChangeEvent;
import com.shayan.notebook.events.MenuItemClickEvent;
import com.shayan.notebook.events.NoteListChangeEvent;
import com.shayan.notebook.events.ShowFragmentEvent;
import com.shayan.notebook.model.NoteFolder;
import com.shayan.notebook.model.NoteItem;
import com.shayan.notebook.util.CommonUtils;
import com.shayan.notebook.view.fragment.base.BaseFragment;
import com.shayan.notebook.viewmodel.NoteViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class NoteFragment extends BaseFragment {

    private FragmentNoteBinding binding;
    private NoteViewModel viewModel;

    @Setter
    private NoteItem noteItem;
    @Setter
    private NoteFolder noteFolder;

    @Inject
    Config config;

    public static NoteFragment create(NoteFolder folder) {
        NoteFragment fragment = new NoteFragment();
        fragment.setNoteFolder(folder);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        App.get(getActivity()).getServiceComponent().inject(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_note, container, false);
        ButterKnife.bind(this, binding.getRoot());
        viewModel = new NoteViewModel(getActivity());
        binding.setViewModel(viewModel);

        if (noteItem != null) {
            setValues();
        } else {
            noteItem = new NoteItem(noteFolder.getId());
            //generate id for object
            noteItem = viewModel.insertNote(noteItem);
        }

        return binding.getRoot();
    }

    private void setValues() {
        binding.noteTitle.setText(noteItem.getTitle());

        if (!TextUtils.isEmpty(noteItem.getContent())) {
            binding.noteContent.setText(noteItem.getContent());
        }

        initDrawing();
    }

    private void initDrawing() {
        if (viewModel.hasDrawing(noteItem)) {
            binding.noteImage.setVisibility(View.VISIBLE);
            binding.divider.setVisibility(View.VISIBLE);
            binding.noteImage.loadImage(noteItem.getDrawingPath(config));
        } else {
            binding.noteImage.setVisibility(View.GONE);
            binding.divider.setVisibility(View.GONE);
        }
    }

    @Subscribe(sticky = true)
    public void onDrawingChangeEvent(DrawingChangeEvent event) {
        EventBus.getDefault().removeStickyEvent(DrawingChangeEvent.class);
        initDrawing();
    }

    /**
     * Called from parent activity
     */
    @Subscribe
    public void onMenuItemClick(MenuItemClickEvent event) {
        MenuItem item = event.getItem();
        switch (item.getItemId()) {
            case R.id.action_drawing:
                showDrawingFragment();
                break;

            case R.id.action_bold:
                binding.noteContent.changeBoldState();
                int iconResBold = binding.noteContent.isBoldEnabled() ? R.mipmap.ic_format_bold_black : R.mipmap.ic_format_bold_white;
                item.setIcon(iconResBold);
                break;

            case R.id.action_italic:
                binding.noteContent.changeItalicState();
                int iconResItalic = binding.noteContent.isItalicEnabled() ? R.mipmap.ic_format_italic_black : R.mipmap.ic_format_italic_white;
                item.setIcon(iconResItalic);
                break;
        }
    }

    @OnClick(R.id.note_image)
    public void onImageClick() {
        showDrawingFragment();
    }

    private void showDrawingFragment() {
        DrawFragment drawFragment = DrawFragment.create(noteItem);
        EventBus.getDefault().post(new ShowFragmentEvent(drawFragment));
    }

    private void updateNote() {
        noteItem.setTitle(binding.noteTitle.getText().toString());
        noteItem.setContent(binding.noteContent.getText());

        viewModel.updateNote(noteItem);
    }

    private boolean isNoteEmpty() {
        return CommonUtils.isEmpty(binding.noteTitle) && CommonUtils.isEmpty(binding.noteContent) && !viewModel.hasDrawing(noteItem);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        inflateActivityMenu(R.menu.note_edit);
        changeActivityTitle(R.string.note);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewModel.onDestroy();
    }

    @Override
    public void onDestroy() {
        if (!isNoteEmpty()) {
            updateNote();
        } else {
            viewModel.deleteNote(noteItem.getId());
        }

        //refresh NoteListFragment's list
        EventBus.getDefault().postSticky(new NoteListChangeEvent());

        super.onDestroy();
    }
}
