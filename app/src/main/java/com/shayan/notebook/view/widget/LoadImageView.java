package com.shayan.notebook.view.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.shayan.notebook.util.StorageUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoadImageView extends ImageView {

    public LoadImageView(Context context) {
        super(context);
    }

    public LoadImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoadImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void loadImage(String filePath) {
        if (!TextUtils.isEmpty(filePath)) {
            StorageUtils.getBitmapFromFile(getContext(), filePath)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::setImageBitmap
                            , Throwable::printStackTrace);
        }
    }

    public void clearImage() {
        setImageBitmap(null);
    }
}
