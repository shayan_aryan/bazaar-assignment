package com.shayan.notebook.view.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shayan.notebook.R;
import com.shayan.notebook.adapter.NotesAdapter;
import com.shayan.notebook.databinding.FragmentNoteListBinding;
import com.shayan.notebook.events.NoteListChangeEvent;
import com.shayan.notebook.events.ShowFragmentEvent;
import com.shayan.notebook.helper.SwipeCallback;
import com.shayan.notebook.helper.recyclerview.DividerDecoration;
import com.shayan.notebook.helper.recyclerview.RecyclerItemClickListener;
import com.shayan.notebook.model.NoteFolder;
import com.shayan.notebook.model.NoteItem;
import com.shayan.notebook.view.fragment.base.BaseFragment;
import com.shayan.notebook.viewmodel.NoteListViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class NoteListFragment extends BaseFragment implements NoteListViewModel.DataListener {

    private FragmentNoteListBinding binding;
    private NoteListViewModel viewModel;
    private NotesAdapter notesAdapter;
    @Setter
    private NoteFolder folder;

    private boolean firstTime = true;

    public static NoteListFragment create(NoteFolder folder) {
        NoteListFragment fragment = new NoteListFragment();
        fragment.setFolder(folder);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_note_list, container, false);
        initViews();
        ButterKnife.bind(this, binding.getRoot());

        viewModel = new NoteListViewModel(getActivity(), this, folder.getId());
        setValues();
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    private void initViews() {
        RecyclerView recyclerView = binding.rvNotes;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerDecoration.Builder(getActivity())
                        .setHeight(R.dimen.global_padding_smaller)
                        .setColor(Color.TRANSPARENT)
                        .build());

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), (view, position) -> {
                    NoteItem item = notesAdapter.getItem(position);
                    showNoteFragment(item);
                })
        );

        notesAdapter = new NotesAdapter(getActivity());
        recyclerView.setAdapter(notesAdapter);

        ItemTouchHelper touchHelper = new ItemTouchHelper(new SwipeCallback(position -> {
            NoteItem item = notesAdapter.getItem(position);
            viewModel.deleteNote(item.getId());
            notesAdapter.removeItem(position);
        }));
        touchHelper.attachToRecyclerView(recyclerView);
    }

    private void setValues() {
        if (firstTime) {
            firstTime = false;
            viewModel.fetchItems();
        }
    }

    @Subscribe(sticky = true)
    public void onNoteListChanged(NoteListChangeEvent noteListChangeEvent) {
        viewModel.fetchItems();
        EventBus.getDefault().removeStickyEvent(NoteListChangeEvent.class);
    }

    @Override
    public void onDataChanged(List<NoteItem> noteItems) {
        notesAdapter.updateItems(noteItems);
    }

    @OnClick(R.id.note_add_btn)
    public void onAddNoteClicked(View view) {
        showNoteFragment(null);
    }

    private void showNoteFragment(@Nullable NoteItem item) {
        NoteFragment noteFragment = NoteFragment.create(folder);
        if (item != null) {
            noteFragment.setNoteItem(item);
        }
        EventBus.getDefault().post(new ShowFragmentEvent(noteFragment));
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        clearActivityMenu();
        changeActivityTitle(folder.getName());
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewModel.onDestroy();
    }
}
