package com.shayan.notebook.view.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.shayan.notebook.R;
import com.shayan.notebook.databinding.FragmentDrawBinding;
import com.shayan.notebook.events.DrawingChangeEvent;
import com.shayan.notebook.events.MenuItemClickEvent;
import com.shayan.notebook.model.NoteItem;
import com.shayan.notebook.view.fragment.base.BaseFragment;
import com.shayan.notebook.view.widget.DrawingView;
import com.shayan.notebook.viewmodel.DrawingViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindColor;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@NoArgsConstructor
public class DrawFragment extends BaseFragment {

    private FragmentDrawBinding binding;
    private DrawingViewModel viewModel;

    @Setter
    private NoteItem noteItem;

    @BindColor(R.color.pink)
    int pink;

    @BindColor(R.color.black_light)
    int blackLight;

    public static DrawFragment create(NoteItem noteItem) {
        DrawFragment drawFragment = new DrawFragment();
        drawFragment.setNoteItem(noteItem);
        return drawFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_draw, container, false);
        ButterKnife.bind(this, binding.getRoot());

        viewModel = new DrawingViewModel(getActivity(), noteItem);
        binding.setViewModel(viewModel);

        initViews();
        return binding.getRoot();
    }

    private void initViews() {
        if (viewModel.hasDrawing()) {
            String drawingPath = viewModel.getDrawingPath();
            binding.drawingView.loadImage(drawingPath);
        }

        onBrushClick();
    }

    @OnClick(R.id.btn_brush)
    public void onBrushClick() {
        binding.btnBrush.setTextColor(pink);
        binding.btnEraser.setTextColor(blackLight);
        binding.drawingView.setMode(DrawingView.Mode.PAINTING);
    }

    @OnClick(R.id.btn_eraser)
    public void onEraserClick() {
        binding.btnEraser.setTextColor(pink);
        binding.btnBrush.setTextColor(blackLight);
        binding.drawingView.setMode(DrawingView.Mode.ERASING);
    }

    @OnClick(R.id.btn_undo)
    public void onUndoClick() {
        binding.drawingView.undo();
    }

    @OnClick(R.id.btn_redo)
    public void onRedoClick() {
        binding.drawingView.redo();
    }

    private void saveDrawing(Bitmap drawing) {
        viewModel.saveDrawing(drawing)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> EventBus.getDefault().postSticky(new DrawingChangeEvent())
                        , Throwable::printStackTrace);
    }

    private void deleteDrawing() {
        binding.drawingView.clear();
        viewModel.deleteDrawing();
    }

    /**
     * Called from parent activity
     */
    @Subscribe
    public void onMenuItemClick(MenuItemClickEvent event) {
        MenuItem item = event.getItem();
        switch (item.getItemId()) {
            case R.id.action_delete:
                deleteDrawing();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        changeActivityTitle(R.string.drawing);
        inflateActivityMenu(R.menu.draw_edit);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);

        Bitmap drawing = binding.drawingView.getDrawing();
        if (drawing != null) {
            //drawing has changed or just been created
            saveDrawing(drawing);
        }

        super.onPause();
    }
}
