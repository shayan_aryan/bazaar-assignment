package com.shayan.notebook.application;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.shayan.notebook.R;
import com.shayan.notebook.db.DataBaseManager;
import com.shayan.notebook.di.component.DaggerServiceComponent;
import com.shayan.notebook.di.component.ServiceComponent;
import com.shayan.notebook.di.module.ServiceModule;
import com.shayan.notebook.helper.PreferenceHelper;

import javax.inject.Inject;

import lombok.Getter;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends MultiDexApplication {

    @Getter
    private ServiceComponent serviceComponent;

    @Inject
    DataBaseManager dataBaseManager;
    @Inject
    PreferenceHelper preferenceHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
        serviceComponent.inject(this);
        initCalligraphy();

        if (preferenceHelper.isFirstLaunch()) {
            dataBaseManager.setupDefaultItems();
        }
    }

    private void initDagger() {
        serviceComponent = DaggerServiceComponent.builder()
                .serviceModule(new ServiceModule(this))
                .build();
    }

    private void initCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.font_roboto_regular))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }

    /**
     * Mock Injections while automated testing
     */
    public void setServiceComponent(ServiceComponent serviceComponent) {
        this.serviceComponent = serviceComponent;
        serviceComponent.inject(this);
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}