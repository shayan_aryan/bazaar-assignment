package com.shayan.notebook.application;


import android.content.Context;
import android.os.Environment;

import com.shayan.notebook.BuildConfig;
import com.shayan.notebook.helper.DateHelper;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import lombok.Getter;

public class Config {

    //directory in which drawings are saved
    @Getter
    private String filesDirectory;
    @Getter
    private char[] archivePass;
    private String backupFileName;

    public Config(Context context) {
        Properties properties = new Properties();
        String configFileName = BuildConfig.DEBUG ? "config.dev.properties" : "config.prod.properties";
        try {
            properties.load(context.getAssets().open(configFileName));

            String filesDirectoryName = properties.getProperty("files.directory");
            createFilesDirectory(context, filesDirectoryName);

            backupFileName = properties.getProperty("backup.file.name");

            archivePass = properties.getProperty("archive.password").toCharArray();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createFilesDirectory(Context context, String filesDirectoryName) {
        filesDirectory = context.getFilesDir()
                .getPath()
                .concat(File.separator)
                .concat(filesDirectoryName)
                .concat(File.separator);

        new File(filesDirectory).mkdirs();
    }

    public String getBackupFilePath(Context context) {
        String fileName = backupFileName
                .concat("_")
                .concat(DateHelper.getFormattedDate(new Date()));

        return new File(Environment.getExternalStorageDirectory(), fileName).getPath();

    }

    //note drawing place is ${filesDirectory}/${noteId}
    public String getNoteDrawingPath(int noteId) {
        return filesDirectory.concat(String.valueOf(noteId));
    }
}
