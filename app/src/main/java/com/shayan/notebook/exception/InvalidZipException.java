package com.shayan.notebook.exception;

/**
 * Will be thrown when user addressed an invalid zip file for backup
 */
public class InvalidZipException extends Exception {

}
