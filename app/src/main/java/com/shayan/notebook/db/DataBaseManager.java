package com.shayan.notebook.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.shayan.notebook.R;
import com.shayan.notebook.application.Config;
import com.shayan.notebook.model.NoteFolder;
import com.shayan.notebook.model.NoteItem;
import com.shayan.notebook.util.StorageUtils;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DataBaseManager extends OrmLiteSqliteOpenHelper {

    public static final String DATABASE_NAME = "notebook";
    private static final int DATABASE_VERSION = 1;

    private Context context;
    private Config config;

    private RuntimeExceptionDao<NoteFolder, Integer> folderDao = null;
    private RuntimeExceptionDao<NoteItem, Integer> noteDao = null;

    public DataBaseManager(Context context, Config config) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        this.config = config;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, NoteFolder.class);
            TableUtils.createTable(connectionSource, NoteItem.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    @Override
    public void close() {
        super.close();
        folderDao = null;
        noteDao = null;
    }

    public void setupDefaultItems() {
        insertFolder(new NoteFolder(context.getString(R.string.default_folder)));
    }

    //region NoteFolder
    private RuntimeExceptionDao<NoteFolder, Integer> getFolderDao() {
        if (folderDao == null) {
            folderDao = getRuntimeExceptionDao(NoteFolder.class);
        }
        return folderDao;
    }

    public NoteFolder insertFolder(NoteFolder noteFolder) {
        noteFolder.updateCreationTime();
        return getFolderDao().createIfNotExists(noteFolder);
    }

    public List<NoteFolder> getAllFoldersSorted() {
        try {
            return getFolderDao()
                    .queryBuilder()
                    .orderBy(NoteFolder.Columns.DATE_CREATED, false)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void deleteFolder(int folderId) {
        getFolderDao().deleteById(folderId);
        deleteNotees(folderId);
    }
    //endregion

    //region NoteItem
    private RuntimeExceptionDao<NoteItem, Integer> getNoteDao() {
        if (noteDao == null) {
            noteDao = getRuntimeExceptionDao(NoteItem.class);
        }
        return noteDao;
    }

    /**
     * inserts note to database
     *
     * @return object with assigned id
     */
    public NoteItem insertNote(NoteItem noteItem) {
        noteItem.updateCreationTime();
        return getNoteDao().createIfNotExists(noteItem);
    }

    public void updateNote(NoteItem noteItem) {
        getNoteDao().update(noteItem);
    }

    public NoteItem getNote(int noteId) {
        return getNoteDao().queryForId(noteId);
    }

    public List<NoteItem> getNotesOfFolder(int folderId) {
        try {
            QueryBuilder<NoteItem, Integer> queryBuilder = getNoteDao().queryBuilder();
            queryBuilder.where().eq(NoteItem.Columns.FOLDER_ID, folderId);
            queryBuilder.orderBy(NoteItem.Columns.CREATED_DATE, false);
            return queryBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void deleteNote(int noteId) {
        NoteItem noteItem = getNote(noteId);
        String drawingPath = noteItem.getDrawingPath(config);
        StorageUtils.deleteFile(new File(drawingPath));

        getNoteDao().deleteById(noteId);
    }

    public void deleteNotees(int folderId) {
        try {
            getNoteDao().deleteBuilder()
                    .where().eq(NoteItem.Columns.FOLDER_ID, folderId)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //endregion

}
