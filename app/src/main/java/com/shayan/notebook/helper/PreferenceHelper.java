package com.shayan.notebook.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceHelper {

    private SharedPreferences sharedPreferences;

    private final static String PREF_FIRST_LAUNCH = "firstLaunch";

    public PreferenceHelper(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isFirstLaunch() {
        boolean isFirstLaunch = sharedPreferences.getBoolean(PREF_FIRST_LAUNCH, true);

        if (isFirstLaunch) {
            //set first launch to false
            sharedPreferences.edit().putBoolean(PREF_FIRST_LAUNCH, false).apply();
        }

        return isFirstLaunch;
    }
}
