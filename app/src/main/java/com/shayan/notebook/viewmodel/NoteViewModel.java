package com.shayan.notebook.viewmodel;

import android.content.Context;

import com.shayan.notebook.application.App;
import com.shayan.notebook.application.Config;
import com.shayan.notebook.db.DataBaseManager;
import com.shayan.notebook.model.NoteItem;

import javax.inject.Inject;

/**
 * Created by Shayan on 8/15/16.
 */
public class NoteViewModel implements ViewModel {

    @Inject
    DataBaseManager dataBaseManager;
    @Inject
    Config config;

    public NoteViewModel(Context context) {
        App.get(context).getServiceComponent().inject(this);
    }

    /**
     * inserts note to database
     *
     * @return object with assigned id
     */
    public NoteItem insertNote(NoteItem noteItem) {
        return dataBaseManager.insertNote(noteItem);
    }

    public void updateNote(NoteItem noteItem) {
        dataBaseManager.updateNote(noteItem);
    }

    public NoteItem geteNote(int noteId) {
        return dataBaseManager.getNote(noteId);
    }

    public void deleteNote(int noteId) {
        dataBaseManager.deleteNote(noteId);
    }

    public boolean hasDrawing(NoteItem noteItem) {
        return noteItem.hasDrawing(config);
    }

    @Override
    public void onDestroy() {

    }

}
