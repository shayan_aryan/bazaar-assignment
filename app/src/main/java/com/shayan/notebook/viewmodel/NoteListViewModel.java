package com.shayan.notebook.viewmodel;

import android.content.Context;

import com.shayan.notebook.application.App;
import com.shayan.notebook.db.DataBaseManager;
import com.shayan.notebook.model.NoteItem;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Shayan on 8/14/2016.
 */
public class NoteListViewModel implements ViewModel {

    private DataListener dataListener;
    private int folderId;

    @Inject
    DataBaseManager dataBaseManager;

    public NoteListViewModel(Context context, DataListener dataListener, int folderId) {
        this.dataListener = dataListener;
        this.folderId = folderId;
        App.get(context).getServiceComponent().inject(this);
    }

    public void fetchItems() {
        dataListener.onDataChanged(dataBaseManager.getNotesOfFolder(folderId));
    }

    public void deleteNote(int id) {
        dataBaseManager.deleteNote(id);
    }

    @Override
    public void onDestroy() {
        dataListener = null;
    }

    public interface DataListener{
        void onDataChanged(List<NoteItem> noteItems);
    }
}
