package com.shayan.notebook.viewmodel;

import android.content.Context;
import android.graphics.Bitmap;

import com.shayan.notebook.application.App;
import com.shayan.notebook.application.Config;
import com.shayan.notebook.model.NoteItem;
import com.shayan.notebook.util.StorageUtils;

import java.io.File;

import javax.inject.Inject;

import rx.Observable;

public class DrawingViewModel implements ViewModel {

    private Context context;
    private NoteItem noteItem;

    @Inject
    Config config;

    public DrawingViewModel(Context context, NoteItem noteItem) {
        this.context = context;
        this.noteItem = noteItem;
        App.get(context).getServiceComponent().inject(this);
    }

    public Observable<Integer> saveDrawing(Bitmap bitmap) {
        return StorageUtils.saveBitmapToFile(bitmap, noteItem.getDrawingPath(config));
    }

    public void deleteDrawing() {
        String filePath = noteItem.getDrawingPath(config);
        StorageUtils.deleteFile(new File(filePath));
    }

    public String getDrawingPath() {
        return noteItem.getDrawingPath(config);
    }

    public boolean hasDrawing() {
        return noteItem.hasDrawing(config);
    }

    @Override
    public void onDestroy() {

    }
}
