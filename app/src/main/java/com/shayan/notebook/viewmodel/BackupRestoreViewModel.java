package com.shayan.notebook.viewmodel;

import android.content.Context;
import android.net.Uri;

import com.shayan.notebook.R;
import com.shayan.notebook.application.App;
import com.shayan.notebook.application.Config;
import com.shayan.notebook.util.CommonUtils;
import com.shayan.notebook.util.StorageUtils;
import com.trello.rxlifecycle.FragmentEvent;
import com.trello.rxlifecycle.RxLifecycle;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

public class BackupRestoreViewModel implements ViewModel {

    private Context context;
    private DataListener dataListener;
    private Observable<FragmentEvent> lifecycleSubject = BehaviorSubject.create();

    @Inject
    Config config;

    public BackupRestoreViewModel(Context context, DataListener dataListener, Observable<FragmentEvent> lifecycleSubject) {
        this.context = context;
        this.dataListener = dataListener;
        this.lifecycleSubject = lifecycleSubject;
        App.get(context).getServiceComponent().inject(this);
    }

    public void backupAppData() {
        StorageUtils.backupAppDataDirectory(context, config)
                .compose(RxLifecycle.bindUntilFragmentEvent(lifecycleSubject, FragmentEvent.DESTROY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(file -> {
                            dataListener.onBackupCompleted(true, context.getString(R.string.backed_up_successfully, file.getPath()));
                        }, throwable -> {
                            dataListener.onBackupCompleted(false, context.getString(R.string.backup_failed));
                            throwable.printStackTrace();
                        }
                );
    }

    public void restoreAppData(Uri fileUri) {
        String filePath = CommonUtils.getRealPathFromURI(context, fileUri);

        StorageUtils.restoreAppDataFromFile(context, filePath, config)
                .compose(RxLifecycle.bindUntilFragmentEvent(lifecycleSubject, FragmentEvent.DESTROY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> {
                    dataListener.onRestoreCompleted(true, context.getString(R.string.restored_successfully));
                }, throwable -> {
                    dataListener.onRestoreCompleted(false, context.getString(R.string.restore_failed));
                    throwable.printStackTrace();
                });
    }


    @Override
    public void onDestroy() {
        context = null;
        dataListener = null;
    }

    public interface DataListener {

        void onRestoreCompleted(boolean success, String message);

        void onBackupCompleted(boolean success, String message);
    }
}
