package com.shayan.notebook.viewmodel;

public interface ViewModel {
    void onDestroy();
}
