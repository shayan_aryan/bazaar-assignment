package com.shayan.notebook.viewmodel;

import android.content.Context;

import com.shayan.notebook.application.App;
import com.shayan.notebook.db.DataBaseManager;
import com.shayan.notebook.model.NoteFolder;

import java.util.List;

import javax.inject.Inject;

public class FoldersViewModel implements ViewModel {

    private Context context;
    private DataListener dataListener;

    @Inject
    DataBaseManager dataBaseManager;

    public FoldersViewModel(Context context, DataListener dataListener) {
        this.context = context;
        this.dataListener = dataListener;
        App.get(context).getServiceComponent().inject(this);

        getAllFolders();
    }

    private void getAllFolders() {
        dataListener.onDataChanged(dataBaseManager.getAllFoldersSorted());
    }

    public void addFolder(String folderName) {
        NoteFolder noteFolder = dataBaseManager.insertFolder(new NoteFolder(folderName));
        dataListener.onFolderAdded(noteFolder);
    }

    public void deleteFolder(int folderId) {
        dataBaseManager.deleteFolder(folderId);
    }

    @Override
    public void onDestroy() {
        context = null;
        dataListener = null;
    }

    public interface DataListener {

        void onDataChanged(List<NoteFolder> noteFolders);

        void onFolderAdded(NoteFolder noteFolder);
    }
}
