package com.shayan.notebook.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Getter;

@DatabaseTable(tableName = "note_folder")
@Getter
public class NoteFolder {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField
    private long dateCreated;

    public NoteFolder() {
        //used by ormlite
    }

    public NoteFolder(String name) {
        this.name = name;
    }

    public void updateCreationTime() {
        dateCreated = System.currentTimeMillis();
    }


    public class Columns{
        public static final String NAME = "name";
        public static final String DATE_CREATED = "dateCreated";
    }
}
