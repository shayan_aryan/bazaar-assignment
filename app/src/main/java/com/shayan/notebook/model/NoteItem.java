package com.shayan.notebook.model;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.shayan.notebook.application.Config;

import java.io.File;
import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@DatabaseTable(tableName = "note_item")
public class NoteItem implements Serializable {

    @DatabaseField(generatedId = true)
    @Getter
    private int id;

    @DatabaseField
    @Setter
    @Getter
    private String title;

    @DatabaseField
    private String content;

    @DatabaseField
    @Setter
    @Getter
    private int folderId;

    @DatabaseField
    @Setter
    @Getter
    private long createdDate;

    public NoteItem() {
        //used by ormlite
    }

    public NoteItem(int folderId) {
        this.folderId = folderId;
    }

    public Spanned getContent() {
        if (!TextUtils.isEmpty(content)) {
            //loading text with styles by using html
            return Html.fromHtml(content);
        } else
            return null;
    }

    public void setContent(Spanned spanned) {
        //saving styles by using html
        content = Html.toHtml(spanned);
    }

    public void updateCreationTime() {
        createdDate = System.currentTimeMillis();
    }

    public boolean hasDrawing(Config config) {
        return new File(config.getNoteDrawingPath(id)).exists();
    }

    public String getDrawingPath(Config config) {
        return config.getNoteDrawingPath(id);
    }

    public class Columns {
        public static final String FOLDER_ID = "folderId";
        public static final String CREATED_DATE = "createdDate";
    }
}
